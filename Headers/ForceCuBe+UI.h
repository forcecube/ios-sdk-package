//
//  ForceCuBe+UI.h
//  ForceCuBe
//
//  Created by Stanislav Olekhnovich on 28/07/16.
//  Copyright © 2016 Stanislav Olekhnovich. All rights reserved.
//

#import "ForceCuBe.h"

@class FCBCampaignOfferViewController, FCBAcceptedOfferViewController, FCBInfoAndSettingsController;
@class FCBAdvertisersFilterController;

@interface ForceCuBe (UI)

/**
 Returns instance of FCBCampaignOfferViewController.
 
 @param campaignId Campaign id, which was sent in notification.
 */
- (FCBCampaignOfferViewController *) campaignOfferControllerWithCampaignId: (NSUInteger) campaignId;

/**
 Returns instance of FCBAcceptedOfferViewController.
 
 @param campaignId Accepted campaign id.
 */
- (FCBAcceptedOfferViewController *) acceptedOfferControllerWithCampaignId: (NSUInteger) campaignId;

/**
 Returns instance of FCBInfoAndSettingsController.
 */
- (FCBInfoAndSettingsController *) infoAndSettingsController;

/**
 Returns instance of FCBAdvertisersFilterController.
 */
- (FCBAdvertisersFilterController *) advertisersFilterController;

@end
