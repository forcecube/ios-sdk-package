//
//  ForceCuBe.h
//  ForceCuBe
//
//  Created by Stanislav Olekhnovich on 24/12/14.
//  Copyright (c) 2014 Stanislav Olekhnovich. All rights reserved.
//

#import <Foundation/Foundation.h>

#import "FCBDelegate.h"
#import "FCBStatus.h"

@protocol FCBCampaignManager, FCBLocationsManager, FCBDataWriter, FCBRuntimeSettings;
@protocol FCBNotificationSettings, FCBAppUserManager, FCBAdvertisersFilterManager;


/**
 The ForceCuBe class is the central point for configuring the delivery of
 ForceCuBe beacon-based notifications to your app.
 */
@interface ForceCuBe : NSObject

/**
 Current status of ForceCuBe SDK.
 */
@property (readonly) FCBStatus status;

/**
 Delegate, which is to handle all ForceCuBe SDK events.
 */
@property (weak) id<FCBDelegate> delegate;

/**
 Creates an instance of ForceCuBe.
 
 @param appId Application id given to app developer by ForceCuBe.
 @param appSecret Token, which is used to authorize this app instance on FCB server.
 @param externalId Identifier, which will be used to match any external data with ForceCuBe's appUser record.
 */
- (instancetype) initWithAppDeveloperKey: (NSString *) appId
                      appDeveloperSecret: (NSString *) appSecret
                              externalId: (NSString *) externalId;

/**
 Starts BLE-beacons signal scanning and handling process.
 */
- (void) start;

/**
 Stops BLE-beacons signal scanning and handling process.
 */
- (void) stop;

/**
 SDK's runtime settings.
 */
@property (readonly) id<FCBRuntimeSettings> runtimeSettigs;

/**
 Campaign management API. Use it to get lists of campaign offers. Change status of offers.
 */
@property (readonly) id<FCBCampaignManager> campaignManager;

/**
 Data writing API. Use it to get access to the FCB data writer.
 */
@property (readonly) id<FCBDataWriter> dataWriter;

/**
 Locations API. Use it to locations in which an offer can be redeemed.
 */
@property (readonly) id<FCBLocationsManager> locationsManager;


/**
 Advertisers filter API. Use it get list of advertisers, to set/unset allowed advertisers.
 */
@property (readonly) id<FCBAdvertisersFilterManager> advertisersFilterManager;

/**
 Notifications settings API. Use it to set an allowed frequency of notifications, shown to a user.
 */
@property (readonly) id<FCBNotificationSettings> notificationsSettings;




@end
