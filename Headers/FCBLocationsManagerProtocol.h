//
//  FCBLocationsManagerProtocol.h
//  ForceCuBe
//
//  Created by Stanislav Olekhnovich on 18/07/16.
//  Copyright © 2016 Stanislav Olekhnovich. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol FCBLocation;

/**
 FORCECUBE locations managament API gateway.
 */
@protocol FCBLocationsManager <NSObject>

/**
 Returns location bu id.
 
 @returns An object implementing FCBLocation protocol.
 */
- (id<FCBLocation>) locationById: (NSUInteger) locationId;

/**
 Returns a list of locations in which an offer with campaignOfferId can be redeemed.
 
 @param campaignOfferId Offer id.
 
 @returns An list of objects implementing FCBLocation protocol.
 */
- (NSArray<id<FCBLocation>> *) locationsForCampaignOfferWithId: (NSUInteger) campaignOfferId;

@end
