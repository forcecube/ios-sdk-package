//
//  FCBStatus.h
//  ForceCuBe
//
//  Created by Stanislav Olekhnovich on 15/01/15.
//  Copyright (c) 2015 Stanislav Olekhnovich. All rights reserved.
//

/**
 Status of ForceCuBe SDK.
 */
typedef NS_ENUM(NSInteger, FCBStatus)
{
    /**
     Initial and final state.
     */
    FCBStatusStopped,
    /**
     SDK is just starting. It may require some time to get needed server data to start.
     */
    FCBStatusStarting,
    /**
     SDK has started, but is unable to use beacon-based proximity and will use geo-fencing.
     */
    FCBStatusStartedWithGeofencing,
    /**
     SDK has started and will deliver beacon-based events.
     */
    FCBStatusStarted
};

