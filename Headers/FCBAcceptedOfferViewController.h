//
//  FCBAcceptedOfferViewController.h
//  ForceCuBe
//
//  Created by Stanislav Olekhnovich on 21/01/16.
//  Copyright © 2016 Stanislav Olekhnovich. All rights reserved.
//

#import <UIKit/UIKit.h>

@class FCBAcceptedOfferViewController, ForceCuBe;

/**
 The FCBAcceptedOfferViewControllerDelegate protocol defines callbacks received
 from FCBAcceptedOfferViewController object.
 */
@protocol FCBAcceptedOfferViewControllerDelegate <NSObject>

/**
 Tells a delegate, that a FCBAcceptedOfferViewController has fulfilled it's duty and should be dismissed.
 
 @param controller The FCBAcceptedOfferViewController object
 @param wasOfferAccepted Indicates that offer was check in by the user and
 thus it will be removed from ForceCuBe's acceptedOffers list.
 */
- (void) acceptedOfferViewControllerDidComplete: (FCBAcceptedOfferViewController *) controller
                                didCheckInOffer: (BOOL) didCheckInOffer;

@end


/**
 The FCBAcceptedOfferViewController class allows user to check in an offer (a coupon)
 */
@interface FCBAcceptedOfferViewController : UIViewController

/**
 A delegate.
 */
@property (assign) id<FCBAcceptedOfferViewControllerDelegate> delegate;

/**
 Id of a campaign offer, which will be presented by the controller to a user to check it in.
 Must be set before showing controller to a user.
 */
@property NSUInteger acceptedOfferCampaignId;

/**
 ForceCuBe object, which is used to fetch campaign offer info.
 Must be set before showing controller to a user.
 */
@property ForceCuBe * forcecube;

@end
