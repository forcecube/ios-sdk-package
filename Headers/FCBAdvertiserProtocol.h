//
//  FCBAdvertiserProtocol.h
//  ForceCuBe
//
//  Created by Stanislav Olekhnovich on 19/07/16.
//  Copyright © 2016 Stanislav Olekhnovich. All rights reserved.
//

#import <Foundation/Foundation.h>

/**
 An advertiser.
 */
@protocol FCBAdvertiser <NSObject>

/**
 Id
 */
@property (assign, readonly) NSUInteger advertiserId;
/**
 Advertiser name
 */
@property (readonly) NSString * name;
/**
 Is advertiser allowed to send ads or not.
 */
@property (readonly) BOOL isEnabled;

@end
