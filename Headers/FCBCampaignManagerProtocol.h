//
//  FCBCampaignManagerProtocol.h
//  ForceCuBe
//
//  Created by Stanislav Olekhnovich on 18/07/16.
//  Copyright © 2016 Stanislav Olekhnovich. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol FCBCampaignManager, FCBCampaignOffer, FCBAdvertiser, FCBLocation;


/**
 FCBCampaignManagerDelegate protocol.
 */
@protocol FCBCampaignManagerDelegate <NSObject>

/**
 Tells a delegate, that a an SDK did deliver a campaign offer. This method will be called after a local notification is triggered.
 */
- (void) manager: (id<FCBCampaignManager>) campaignManager
    didDeliverCampaignOffer: (id<FCBCampaignOffer>) campaignOffer;

/**
 Tells a delegate, that there are one or more accepted offers, which can be redeemed at the current user location. App can show a notification,
 reminding user to visit stores and use those offers.
 
 @param campaignManager  A campaign manager
 @param campaignOffers A list of offers, which can be used at the current location
 @param location Current location object, which contains location name
 @param advertisers An array, which contains all advertisers for offers, use offer.advertiserId to find which is which
 */
- (void) manager: (id<FCBCampaignManager>) campaignManager
hasAcceptedOffers: (NSArray<id<FCBCampaignOffer>> *) campaignOffers
forCurrentLocation: (id<FCBLocation>) location
     advertisers: (NSArray<id<FCBAdvertiser>> *) advertisers;

@end


/**
 FORCECUBE campaign management API.
 */
@protocol FCBCampaignManager <NSObject>

/**
 A delegate.
 */
@property id<FCBCampaignManagerDelegate> delegate;

/**
 Sets offer status to kFCBCampaignStatusOpened.
 
 @param campaignOfferId Offer id.
 */
- (void) setCampaignOfferAsOpened: (NSUInteger) campaignOfferId;

/**
 Sets offer status to kFCBCampaignStatusDeclined.
 
 @param campaignOfferId Offer id.
 */
- (void) setCampaignOfferAsDeclined: (NSUInteger) campaignOfferId;

/**
 Sets offer status to kFCBCampaignStatusAccepted.
 
 @param campaignOfferId Offer id.
 */
- (void) setCampaignOfferAsAccepted: (NSUInteger) campaignOfferId;

/**
 Sets offer status to kFCBCampaignStatusRedeemed.
 
 @param campaignOfferId Offer id.
 */
- (void) setCampaignOfferAsRedeemed: (NSInteger) campaignOfferId;

/**
 Returns campaign offer with provided id or nil if no such offer exists.
 
 @param campaignOfferId Offer id.
 
 @returns Campaign offer.
 */
- (id<FCBCampaignOffer>) campaignOfferById: (NSUInteger) campaignOfferId;

/**
 Returns campaign offer in status kFCBCampaignStatusAccepted with provided id or nil if no such offer exists.
 
 @param campaignOfferId Offer id.
 
 @returns Campaign offer in status kFCBCampaignStatusAccepted.
 */
- (id<FCBCampaignOffer>) acceptedCampaignOfferById: (NSUInteger) campaignId;

/**
 Presented, but not opened offers.
 
 @returns An array of objects, which implement FCBCampaignOffer protocol.
 */
- (NSArray<id<FCBCampaignOffer>> *) unopenedOffers;

/**
 Accepted offers.
 
 @returns An array of objects, which implement FCBCampaignOffer protocol and are in kFCBCampaignStatusAccepted status.
 */
- (NSArray<id<FCBCampaignOffer>> *) acceptedOffers;



@end
