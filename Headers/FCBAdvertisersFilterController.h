//
//  FCBAdvertisersFilterController.h
//  ForceCuBe
//
//  Created by Stanislav Olekhnovich on 27/05/16.
//  Copyright © 2016 Stanislav Olekhnovich. All rights reserved.
//

#import <UIKit/UIKit.h>

@class ForceCuBe;

@interface FCBAdvertisersFilterController : UITableViewController

@property ForceCuBe * forcecube;

@end
